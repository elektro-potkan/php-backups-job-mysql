<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups\Jobs;

use InvalidArgumentException;
use MySQLDump;
use Nette;

use ElektroPotkan\Backups\IJob;


/**
 * MySQL database backup job
 */
class MySQL implements IJob {
	use Nette\SmartObject;
	
	
	/** @var MySQLDump */
	private $dump;
	
	/** @var bool */
	private $compression;
	
	
	/**
	 * Constructor
	 */
	public function __construct(MySQLDump $dump, bool $compression = true){
		$this->dump = $dump;
		$this->compression = $compression;
	} // constructor
	
	/**
	 * Creates backup file
	 * @param string $path - exact full path to file to create
	 */
	public function create(string $path): void {
		$this->dump->tables['*'] = MySQLDump::ALL & (~MySQLDump::DROP);
		
		// MySQLDump changes behavior based on given file extension => check and refuse other than our supported ones
		$ext = '.'.$this->getExtension();
		if(substr($path, -strlen($ext)) !== $ext){
			throw new InvalidArgumentException('Cannot create file with other extension then requested!');
		};
		
		$this->dump->save($path);
	} // create
	
	/**
	 * Returns file extension
	 * @return string - extension of output backup file
	 */
	public function getExtension(): string {
		return $this->compression ? 'sql.gz' : 'sql';
	} // getExtension
} // class MySQL
