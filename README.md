Backups - MySQL dump Job
========================

Backup Job for dumping MySQL databases using dg/mysql-dump.
This package provides the `MySQL` job for use with the main `elektro-potkan/backups` package.


Usage
-----
```php
$db = new mysqli($server, $user, $password, $dbname);
$dump = new MySQLDump($db);

$job = new ElektroPotkan\Backups\Jobs\MySQL($dump);

// register the job ($manager is an instance implementing ElektroPotkan\Backups\IManager)
$manager->addJob('my-db-dump', $job);
```

### Compression
By default, the output backup file will be gzip-compressed.
To disable the compression, pass `false` as second parameter to job constructor.

```php
$job = new ElektroPotkan\Backups\Jobs\MySQL($dump, false);// gzip compression disabled
```


Author
------
Elektro-potkan <git@elektro-potkan.cz>


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).

### Branching
This project uses slightly modified Git-Flow Workflow and Branching Model:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/


License
-------
You may use this program under the terms of either the BSD Zero Clause License or the GNU General Public License (GPL) version 3 or later.

See file [LICENSE](LICENSE.md).
